import { useEffect, useState } from 'react';

import { SvgProps } from '@models/svg-props.model';

const Svg = ({ path, svgClass, containerClass }: SvgProps): JSX.Element => {
  const [element, setElement] = useState<string>(undefined);

  const adjustSvgFile = async (ac: AbortController) => {
    const response = await fetch(path, {
      signal: ac.signal,
      headers: {
        'Content-Type': 'image/svg+xml',
      },
    });
    const svgString = await response.text();
    const doc = new DOMParser();
    const xml = doc.parseFromString(svgString, 'image/svg+xml');
    const svgNode = xml.documentElement;
    if (svgClass) {
      svgNode.classList.add(...svgClass.split(' '));
    }

    const svgXml = new XMLSerializer().serializeToString(svgNode);
    setElement(svgXml);
  };

  useEffect(() => {
    const ac = new AbortController();
    adjustSvgFile(ac);

    return () => ac.abort();
  }, [path]);

  return <div className={containerClass} dangerouslySetInnerHTML={{ __html: element }} />;
};

export default Svg;

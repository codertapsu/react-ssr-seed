export interface SvgProps {
  path: string;
  svgClass?: string;
  containerClass?: string;
}

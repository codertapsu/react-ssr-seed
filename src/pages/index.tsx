// import Head from 'next/head';
// import Image from 'next/image';

// import styles from '../styles/Home.module.scss';

// import dynamic from 'next/dynamic';
// import Head from 'next/head';
import Link from 'next/link';
// import Router from 'next/router';

// Router.events.on('routeChangeStart', url => {
//   // console.log(`Loading: ${url}`);
//   // NProgress.start();
// });
// Router.events.on('routeChangeComplete', () => {
//   // NProgress.done()
// });
// Router.events.on('routeChangeError', () => {
//   // NProgress.done()
// });

// const SignIn = dynamic(() => import('@modules/auth/components/sign-in'), { loading: () => <p>...</p> });

export default function IndexPage() {
  return (
    <ul>
      <li>
        <Link href='/'>
          <a>Home</a>
        </Link>
      </li>
      <li>
        <Link
          href={{
            pathname: '/about',
            query: { id: 'test' },
          }}
        >
          <a>About page</a>
        </Link>
      </li>
      <li>
        <Link
          href={{
            pathname: '/sign-in',
            query: { id: '1' },
          }}
        >
          <a>Sign in</a>
        </Link>
      </li>
      <li>
        <Link href='/[date]/[slug]' as='/my-folder/my-id'>
          <a>Dynamic nested Route</a>
        </Link>
      </li>
      <li>
        <Link href='/[slug]' as='/my-slug'>
          <a>First Route</a>
        </Link>
      </li>
      {/* <li>
        <Link href='/my-folder/[id]' as='/my-folder/my-id'>
          <a>Second Route</a>
        </Link>
      </li> */}
    </ul>
  );
}

import { useRouter } from 'next/router';

export default function DynamicPage() {
  const router = useRouter();
  const {
    query: { date, slug },
  } = router;

  return (
    <div>
      Data: {date} - {slug}
    </div>
  );
}

import type { AppProps } from 'next/app';

import 'tailwindcss/tailwind.css';
import '../styles/global.scss';
import '../styles/main.css';

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return <Component {...pageProps} />;
}
export default MyApp;

import { useState } from 'react';
import ReactDOM from 'react-dom';

// import { useRouter } from 'next/router';
import Svg from '@components/svg';

const Banner = () => {
  return ReactDOM.createPortal(
    <div className='treact-popup fixed inset-0 flex items-center justify-center' style={{ backgroundColor: 'rgba(0,0,0,0.3)' }}>
      <div className='max-w-lg p-8 sm:pb-4 bg-white rounded shadow-lg text-center sm:text-left'>
        <h3 className='text-xl sm:text-2xl font-semibold mb-6 flex flex-col sm:flex-row items-center'>
          <Svg
            path='/static/icons/speaker.svg'
            svgClass='text-green-800 inline-block w-5 h-5'
            containerClass='bg-green-200 p-2 rounded-full flex items-center mb-4 sm:mb-0 sm:mr-2'
          />
          Free TailwindCSS Component Kit!
        </h3>
        <p>
          I recently released Treact, a <span className='font-bold'>free</span> TailwindCSS Component Kit built with React.
        </p>
        <p className='mt-2'>It has 52 different UI components, 7 landing pages, and 8 inner pages prebuilt. And they are customizable!</p>
        <div className='mt-8 pt-8 sm:pt-4 border-t -mx-8 px-8 flex flex-col sm:flex-row justify-end leading-relaxed'>
          <button className='close-treact-popup px-8 py-3 sm:py-2 rounded border border-gray-400 hover:bg-gray-200 transition duration-300'>
            Close
          </button>
          <a
            className='font-bold mt-4 sm:mt-0 sm:ml-4 px-8 py-3 sm:py-2 rounded bg-purple-700 text-gray-100 hover:bg-purple-900 transition duration-300 text-center'
            href='https://treact.owaiskhan.me'
            target='_blank'
          >
            See Treact
          </a>
        </div>
      </div>
    </div>,
    document.getElementById('root_modal'),
  );
};

const SignIn = () => {
  //   const router = useRouter();
  //   const {
  //     query: { id },
  //   } = router;

  // const handleFormSubmit = (e: any) => {
  //   e.preventDefault();

  //   let email = (e.target as any).elements.email?.value;
  //   let password = (e.target as any).elements.password?.value;

  //   console.log(email, password);
  // };

  // const closeTreactPopup = () => {
  //   document.querySelector('.treact-popup').classList.add('hidden');
  // };
  // const openTreactPopup = () => {
  //   document.querySelector('.treact-popup').classList.remove('hidden');
  // };
  // document.querySelector('.close-treact-popup').addEventListener('click', closeTreactPopup);
  // setTimeout(openTreactPopup, 3000);
  const [openModal, setOpenModal] = useState<boolean>(false);

  return (
    <div>
      <div className='lg:flex'>
        <div className='lg:w-1/2 xl:max-w-screen-sm'>
          <div className='py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12'>
            <div className='cursor-pointer flex items-center'>
              <Svg path='/static/icons/logo.svg' svgClass='text-indigo-500 w-10' />
              <div className='text-2xl text-indigo-800 tracking-wide ml-2 font-semibold'>blockify</div>
            </div>
          </div>
          <div className='mt-10 px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl'>
            <h2
              className='text-center text-4xl text-indigo-900 font-display font-semibold lg:text-left xl:text-5xl
              xl:text-bold'
            >
              Log in
            </h2>
            <div className='mt-12'>
              <form>
                <div>
                  <div className='text-sm font-bold text-gray-700 tracking-wide'>Email Address</div>
                  <input
                    className='w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500'
                    type='text'
                    placeholder='mike@gmail.com'
                  />
                </div>
                <div className='mt-8'>
                  <div className='flex justify-between items-center'>
                    <div className='text-sm font-bold text-gray-700 tracking-wide'>Password</div>
                    <div>
                      <a
                        className='text-xs font-display font-semibold text-indigo-600 hover:text-indigo-800
                                  cursor-pointer'
                      >
                        Forgot Password?
                      </a>
                    </div>
                  </div>
                  <input
                    className='w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500'
                    type='password'
                    placeholder='Enter your password'
                  />
                </div>
                <div className='mt-10'>
                  <button
                    className='bg-indigo-500 text-gray-100 p-4 w-full rounded-full tracking-wide
                          font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-indigo-600
                          shadow-lg'
                    type='button'
                    onClick={() => setOpenModal(true)}
                  >
                    Log In
                  </button>
                </div>
              </form>
              <div className='mt-12 text-sm font-display font-semibold text-gray-700 text-center'>
                Don't have an account ? <a className='cursor-pointer text-indigo-600 hover:text-indigo-800'>Sign up</a>
              </div>
            </div>
          </div>
        </div>
        <div className='hidden lg:flex items-center justify-center bg-indigo-100 flex-1 h-screen'>
          <div className='max-w-xs transform duration-200 hover:scale-110 cursor-pointer'>
            <Svg path='/static/icons/welcome.svg' svgClass='w-5/6 mx-auto' />
          </div>
        </div>
      </div>
      {openModal && Banner()}
    </div>
  );
};

// SignIn.getInitialProps = ({ query: { id } }) => {
//   return { id };
// };

export default SignIn;
